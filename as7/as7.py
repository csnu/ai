#import numpy as np
#import math
#Input: the lists of prior probabilities, likelihood, and test data
#Output: list of corresponding posterior probabilities
#
def posteriorFunc(priorProb, likhd, data):
    posProb = []
    alphas = []

    pows_lime = sum(data)
    pows_cherry = len(data) - pows_lime

    for prior_prob, likelihood, d in zip(priorProb, likhd, data):
    	p = pow(likelihood, pows_lime) * pow(1 - likelihood, pows_cherry) * prior_prob
    	
    	alphas.append(p)

    
    alpha = 1 / sum(alphas)
    
    for prob in alphas:
    	posProb.append(alpha * prob)

    return posProb

#Input the lists of prior probabilites, likhd/likelihood, training data, and one test datapoint
#Output: probability that the test datapoint happens
#Note: this function will call posteriorFunc to calculate the posterior probabilites 
def predictionFunc (priorProb, likhd, data, fPoint):
    posProb = posteriorFunc(priorProb, likhd, data)
    predictProb = 0.00
    

    for likelihood, pos_prob in zip(likhd, posProb):
        total = pow(likelihood, fPoint) * pow(1 - likelihood, 1 - fPoint) * pos_prob

        predictProb += total

    return predictProb