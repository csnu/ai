#Assignment 3
from as3_tree import Tree
class Result:
	def __init__(self, sol=[], val=-1000):
			self.solution = sol
			self.value = val

class MNX:
	def __init__(self, data_list):
		self.tree = Tree()
		self.tree.init_tree(data_list)
		self.root = self.tree.root
		self.currentNode = None
		self.successors = []
		self.traversed = []
		self.arr = []
		return

	def terminalTest(self, node):
		assert node is not None
		return len(node.children) == 0

	def utilityChecking(self, node):
		assert node is not None
		return node.value

	def getChildren(self, node):
		assert node is not None
		return node.children

	def minimax(self):
		terminal_val = self.max_v(self.root)

		res=Result()
		for i in range(100):
			self.arr.append(-1)
			self.arr.append(1)


		depth = self.depth(self.root)
		self.traversed.append(self.root.Name)
		
		self.traverse(self.root, terminal_val, depth)


#################  Return the solution here  #################
		res.value=terminal_val #you put the best terminal value for root node here
		res.solution=self.traversed #you put the solution_array here
#################  Return the solution here  #################



		return res

	def traverse(self, node, val, depth):
		children = self.getChildren(node)

		for child in children:
			if self.arr.pop(0) == -1:
				value = self.min_v(child)
			else:
				value = self.max_v(child)

			if value == val:
				self.traversed.append(child.Name)

			self.traverse(child, val, depth-1)


	def depth(self, node):

		try:
			children = self.getChildren(node)
			child = children[0]
			return 1 + self.depth(child)
		except IndexError:
			return 0



	def max_v(self, node):
		if self.terminalTest(node):
			return self.utilityChecking(node)
		max_v = -1000 #we use 1000 as the initial_maximum value
		deeper_layer = self.getChildren(node)
		for deeper_node in deeper_layer:
			max_v = max(max_v, self.min_v(deeper_node))
		return max_v

	def min_v(self, node):
		if self.terminalTest(node):
			return self.utilityChecking(node)
		min_v = 1000 #we use -1000 as the initial_minimum value
		deeper_layer = self.getChildren(node)
		for deeper_node in deeper_layer:
			min_v = min(min_v, self.max_v(deeper_node))
		return min_v
